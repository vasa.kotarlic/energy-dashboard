# Energy consumption

This project is done as part of interview process. It represents dashboard for company that provides energy management solution.
It is implemented using React/Typescript bootstrapped using Vite with supabase as backend.

Live version of project is on: https://energy-dashboard-six.vercel.app/
It may take some time to access it since free plan is used.

Test user to be used:

```
Email: admin@energy-dashboard.com
Password: IamtheAdmin
```

## Running project

To run the project first clone project repository and execute following commands:

```bash
  cd energy-dashboard
  npm install
  npm run dev
```

This will spin up dev server on port 3000

## Technologies used

1. React
2. Typescript
3. Supabase
4. Mantine

## Possible improvements

1. Store Drag and Drop in supabase under user account
2. Extend sidebar elements (Add account management, settings)
3. Improve Login page design
4. Add tests
