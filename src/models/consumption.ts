export interface Consumption {
  id: number;
  consumption: number | null;
  customers: {
    customer_name: string | null;
  } | null;
}
