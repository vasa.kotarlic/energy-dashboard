export interface ReContribution {
  id: number;
  contribution: number | null;
  customers: {
    customer_name: string | null;
  } | null;
}
