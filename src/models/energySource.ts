export interface EnergySource {
  id: number;
  source: string | null;
  percentage: number | null;
}
