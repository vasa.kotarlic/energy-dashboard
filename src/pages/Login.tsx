import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Auth } from '@supabase/auth-ui-react';
import { ThemeSupa } from '@supabase/auth-ui-shared';
import { Session } from '@supabase/supabase-js';
import { supabase } from '../config/supabase';
import { Center, Flex } from '@mantine/core';

const themeSupaExtended = {
  default: {
    colors: {
      brand: '#33658A',
      brandAccent: '#86BBD8',
      brandButtonText: 'white'
    }
  },
  dark: {
    colors: {
      brandButtonText: 'white'
    }
  }
};

const Login: React.FC = () => {
  const [session, setSession] = useState<Session | null>(null);
  const navigate = useNavigate();

  useEffect(() => {
    if (session) {
      navigate('/dashboard');
    }
  }, [navigate, session]);

  useEffect(() => {
    supabase.auth.getSession().then(({ data: { session } }) => {
      if (session) {
        setSession(session);
        sessionStorage.setItem('session', JSON.stringify(session));
      }
    });

    const {
      data: { subscription }
    } = supabase.auth.onAuthStateChange((_event, session) => {
      if (session) {
        setSession(session);
        sessionStorage.setItem('session', JSON.stringify(session));
      }
    });

    return () => subscription.unsubscribe();
  }, []);

  return (
    <Center>
      <Flex direction='column' justify='flex-start' align='center'>
        <h2>Welcome to energy dashboard</h2>
        <Auth
          supabaseClient={supabase}
          appearance={{ theme: ThemeSupa, variables: themeSupaExtended }}
          providers={[]}
        />
      </Flex>
    </Center>
  );
};

export default Login;
