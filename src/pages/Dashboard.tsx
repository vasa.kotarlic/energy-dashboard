import { useCallback, useEffect, useState } from 'react';
import { Card, Center, Flex, Grid, rem } from '@mantine/core';
import {
  DndContext,
  closestCenter,
  DragOverlay,
  useSensor,
  useSensors,
  DragStartEvent,
  DragEndEvent,
  Active,
  MouseSensor,
  TouchSensor
} from '@dnd-kit/core';
import { arrayMove, rectSortingStrategy, SortableContext } from '@dnd-kit/sortable';
import RealtimeConsumptionChart from '../components/charts/RealtimeConsumptionChart';
import RenewableEnergyChart from '../components/charts/RenewableEnergyChart';
import EnergySourcesChart from '../components/charts/EnergySourcesChart';
import { getConsumptionByDate, getTotalConsumption } from '../service/consumptionService';
import EnergyConsumptionChart from '../components/charts/EnergyConsumptionChart';

const Dashboard = () => {
  const [currentlyDragging, setCurrentlyDragging] = useState<Active | null>(null);
  const [totalConsumption, setTotalConsumption] = useState<number | null>(0);
  const [totalRevenue, setTotalRevenue] = useState<number | null>(0);
  const [itemsToRender, setItemsToRender] = useState([
    { id: 1, component: <RealtimeConsumptionChart id={1} key={1} />, focused: true },
    { id: 2, component: <EnergyConsumptionChart id={2} key={2} />, focused: false },
    { id: 3, component: <RenewableEnergyChart id={3} key={3} />, focused: false },
    { id: 4, component: <EnergySourcesChart id={4} key={4} />, focused: false }
  ]);

  const sensors = useSensors(useSensor(MouseSensor), useSensor(TouchSensor));

  const handleDragStart = useCallback((event: DragStartEvent) => {
    setCurrentlyDragging(event.active);
  }, []);

  const handleDragEnd = useCallback(
    (event: DragEndEvent) => {
      const { active, over } = event;

      const activeComponent = itemsToRender.find((x) => x.id === active.id);
      const overComponent = itemsToRender.find((x) => x.id === over!.id);

      if (active.id !== over?.id) {
        setItemsToRender((items) => {
          const oldIndex = itemsToRender.indexOf(activeComponent!);
          const newIndex = itemsToRender.indexOf(overComponent!);

          if (overComponent?.focused) {
            overComponent.focused = false;
            activeComponent!.focused = true;
          }

          return arrayMove(items, oldIndex, newIndex);
        });
      }

      setCurrentlyDragging(null);
    },
    [itemsToRender]
  );

  const handleDragCancel = useCallback(() => {
    setCurrentlyDragging(null);
  }, []);

  useEffect(() => {
    getTotalConsumption().then((data) => {
      if (data) {
        setTotalConsumption(data);
        setTotalRevenue(data * 0.12);
      }
    });

    getConsumptionByDate().then((data) => console.log(data));
  }, []);

  return (
    <Center p={rem(20)}>
      <DndContext
        sensors={sensors}
        collisionDetection={closestCenter}
        onDragStart={handleDragStart}
        onDragEnd={handleDragEnd}
        onDragCancel={handleDragCancel}
      >
        <Grid columns={12}>
          <Grid.Col span={6}>
            <Card shadow='sm' padding='sm' radius='md' withBorder>
              <Flex justify='center' align='center' direction='column'>
                <h4>Total consumption: </h4>
                <span>{totalConsumption} kWh</span>
              </Flex>
            </Card>
          </Grid.Col>
          <Grid.Col span={6}>
            <Card shadow='sm' padding='sm' radius='md' withBorder>
              <Flex justify='center' align='center' direction='column'>
                <h4>Total revenue: </h4>
                <span>$ {totalRevenue} million</span>
              </Flex>
            </Card>
          </Grid.Col>
          <SortableContext items={itemsToRender} strategy={rectSortingStrategy}>
            {itemsToRender.map((item) => (
              <Grid.Col span={6}>{item.component}</Grid.Col>
            ))}
          </SortableContext>
        </Grid>
        <DragOverlay adjustScale style={{ transformOrigin: '0 0' }}>
          {currentlyDragging
            ? itemsToRender.find((x) => x.id === currentlyDragging.id)!.component
            : null}
        </DragOverlay>
      </DndContext>
    </Center>
  );
};

export default Dashboard;
