import { Route, Routes, useNavigate } from 'react-router-dom';
import Dashboard from './pages/Dashboard';
import Layout from './components/Layout/Layout';
import Login from './pages/Login';
import { useEffect } from 'react';

const App = () => {
  const navigate = useNavigate();
  const session = sessionStorage.getItem('session');

  useEffect(() => {
    if (!session) {
      navigate('/');
    }
  }, [navigate, session]);

  return (
    <Layout>
      <Routes>
        <Route path='/' element={<Login />} />
        <Route path='/dashboard' element={<Dashboard />} />
      </Routes>
    </Layout>
  );
};

export default App;
