import { supabase } from '../config/supabase';

export const login = () => {
  supabase.auth.getSession().then(({ data: { session } }) => {
    if (session) {
      sessionStorage.setItem('session', JSON.stringify(session));
    }
  });
};

export const subscribeToSession = () => {
  const {
    data: { subscription }
  } = supabase.auth.onAuthStateChange((_event, session) => {
    if (session) {
      sessionStorage.setItem('session', JSON.stringify(session));
    }
  });

  return subscription;
};
