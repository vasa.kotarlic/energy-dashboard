import { supabase } from '../config/supabase';

export const getEnergySources = async () => {
  const { data } = await supabase.from('energy_sources').select(`id, source, percentage`);

  return data;
};
