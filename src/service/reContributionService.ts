import { supabase } from '../config/supabase';

export const getReContribution = async () => {
  const { data } = await supabase
    .from('re_contribution')
    .select(`id, contribution, customers (customer_name)`);

  return data;
};
