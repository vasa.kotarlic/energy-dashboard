import dayjs from 'dayjs';
import { supabase } from '../config/supabase';

export const getConsumption = async () => {
  const { data } = await supabase
    .from('consumption')
    .select(`id, consumption, customers (customer_name)`);

  return data;
};

export const getConsumptionByIndustry = async () => {
  const { data } = await supabase
    .from('consumption')
    .select(`id, consumption, customers (customer_name)`);

  const response = new Map<string, number>();

  data?.forEach((x) => {
    const customerName = x.customers?.customer_name ?? '';
    if (!response.has(customerName)) {
      response.set(customerName, x.consumption!);
    } else {
      response.set(customerName, response.get(customerName) ?? 0 + x.consumption!);
    }
  });

  return response;
};

export const getConsumptionByDate = async () => {
  const { data } = await supabase
    .from('consumption')
    .select(`id, consumption, created_at, customers (customer_name)`);

  const response = new Map<string, number>();

  data?.forEach((x) => {
    const date = dayjs(x.created_at).format('DD-MM-YYYY');
    if (!response.has(date)) {
      response.set(date, x.consumption ?? 0);
    } else {
      response.set(date, response.get(x.created_at) ?? 0 + x.consumption!);
    }
  });

  return response;
};

export const getTotalConsumption = async () => {
  const { data } = await supabase.rpc('totalConsumption');
  return data;
};
