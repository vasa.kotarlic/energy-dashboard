import { Button as MantineButton, rem } from '@mantine/core';

interface ButtonProps {
  label: string;
  size: 'sm' | 'md' | 'lg';
  fullWidth?: boolean;
  handleClick?: () => void;
}

const Button: React.FC<ButtonProps> = ({ label, size, fullWidth, handleClick }) => {
  const styles = {
    root: { paddingRight: rem(14), height: rem(48), width: fullWidth ? '100%' : '' },
    section: { marginLeft: rem(22) }
  };

  return (
    <MantineButton radius='xl' size={size} styles={styles} onClick={handleClick}>
      {label}
    </MantineButton>
  );
};

export default Button;
