import { TextInput } from '@mantine/core';

interface InputProps {
  label: string;
  placeholder?: string;
  size: 'sm' | 'md' | 'lg';
  type: 'password' | 'text';
  fullWidth?: boolean;
  handleChange?: () => void;
}

const Input: React.FC<InputProps> = ({
  label,
  placeholder,
  type,
  handleChange,
  size,
  fullWidth
}) => {
  return (
    <TextInput
      type={type}
      label={label}
      placeholder={placeholder}
      size={size}
      onChange={handleChange}
      style={fullWidth ? { width: '100%' } : {}}
    />
  );
};

export default Input;
