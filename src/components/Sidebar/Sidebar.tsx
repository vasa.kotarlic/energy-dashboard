import { Center, Stack, Tooltip, rem } from '@mantine/core';
import { IconGauge, IconSettings, IconLogout, IconSolarPanel2 } from '@tabler/icons-react';
import classes from './Sidebar.module.css';
import SidebarLink from '../SidebarLink/SidebarLink';
import { useLocation, useNavigate } from 'react-router-dom';
import { supabase } from '../../config/supabase';

const sidebarLinks = [
  { icon: IconGauge, label: 'Dashboard', path: '/dashboard' },
  { icon: IconSettings, label: 'Settings' }
];

const Sidebar: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const links = sidebarLinks.map((link) => (
    <SidebarLink
      {...link}
      key={link.label}
      active={link.path === location.pathname}
      onClick={link.path ? () => navigate(link.path) : () => {}}
    />
  ));

  const handleLogout = async () => {
    await supabase.auth.signOut();
    sessionStorage.clear();
    navigate('/');
  };

  return (
    <nav className={classes['navbar']}>
      <Tooltip label='Energy dashboard' position='right' transitionProps={{ duration: 0 }}>
        <Center mb={rem(25)}>
          <IconSolarPanel2 color='#04364A' />
        </Center>
      </Tooltip>

      <div className={classes['navbar-main']}>
        <Stack justify='center' gap={10}>
          {links}
        </Stack>
      </div>

      <Stack justify='center' gap={10}>
        <SidebarLink icon={IconLogout} label='Logout' onClick={handleLogout} />
      </Stack>
    </nav>
  );
};

export default Sidebar;
