import { useEffect, useState } from 'react';
import ChartWrapper from '../ChartWrapper/ChartWrapper';
import { getConsumptionByDate } from '../../service/consumptionService';

interface EnergyConsumptionChartProps {
  id: number;
}

const EnergyConsumptionChart: React.FC<EnergyConsumptionChartProps> = ({ id }) => {
  const [consumption, setConsumption] = useState<Map<string, number>>(new Map());

  useEffect(() => {
    getConsumptionByDate().then((data) => setConsumption(data));
  }, []);

  return (
    <ChartWrapper
      id={id}
      chartName='Energy consumption by date'
      data={[...consumption!.values()]}
      labels={[...consumption!.keys()]}
      type='line'
    />
  );
};

export default EnergyConsumptionChart;
