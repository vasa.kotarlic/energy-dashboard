import { useState, useEffect } from 'react';
import ChartWrapper from '../ChartWrapper/ChartWrapper';
import { getReContribution } from '../../service/reContributionService';
import { ReContribution } from '../../models/reContribution';

interface RenewableEnergyChartProps {
  id: number;
}

const RenewableEnergyChart: React.FC<RenewableEnergyChartProps> = ({ id }) => {
  const [reContribution, setReContribution] = useState<ReContribution[] | null>([]);

  useEffect(() => {
    getReContribution().then((data) => setReContribution(data));
  }, []);

  return (
    <ChartWrapper
      id={id}
      chartName='Renewable Energy contribution'
      data={reContribution!.map((c) => (c.contribution ? c.contribution : 0))}
      labels={reContribution!.map((c) =>
        c.customers!.customer_name ? c.customers!.customer_name : ''
      )}
      type='pie'
    />
  );
};

export default RenewableEnergyChart;
