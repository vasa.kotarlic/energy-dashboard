import { useCallback, useEffect, useState } from 'react';
import { Consumption } from '../../models/consumption';
import { getConsumption } from '../../service/consumptionService';
import ChartWrapper from '../ChartWrapper/ChartWrapper';
import { supabase } from '../../config/supabase';

interface RealtimeConsumptionChartProps {
  id: number;
}

const RealtimeConsumptionChart: React.FC<RealtimeConsumptionChartProps> = ({ id }) => {
  const [consumption, setConsumption] = useState<Consumption[] | null>([]);

  useEffect(() => {
    getConsumption().then((data) => setConsumption(data));
  }, []);

  const handleRealtimeUpdate = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (payload: any) => {
      setConsumption([...consumption!, payload.new as Consumption]);
    },
    [consumption]
  );

  useEffect(() => {
    const subscription = supabase
      .channel('consumption')
      .on(
        'postgres_changes',
        { event: 'INSERT', schema: 'public', table: 'consumption' },
        handleRealtimeUpdate
      )
      .subscribe();

    () => subscription.unsubscribe();
  }, [handleRealtimeUpdate]);

  return (
    <ChartWrapper
      id={id}
      chartName='Realtime energy consumption'
      data={consumption!.map((c) => (c.consumption ? c.consumption : 0))}
      labels={consumption!.map((c) =>
        c.customers!.customer_name ? c.customers!.customer_name : ''
      )}
      type='line'
    />
  );
};

export default RealtimeConsumptionChart;
