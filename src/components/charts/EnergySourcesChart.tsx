import { useState, useEffect } from 'react';
import { EnergySource } from '../../models/energySource';
import { getEnergySources } from '../../service/energySources';
import ChartWrapper from '../ChartWrapper/ChartWrapper';

interface EnergySourcesChartProps {
  id: number;
}

const EnergySourcesChart: React.FC<EnergySourcesChartProps> = ({ id }) => {
  const [consumption, setConsumption] = useState<EnergySource[] | null>([]);

  useEffect(() => {
    getEnergySources().then((data) => setConsumption(data));
  }, []);

  return (
    <ChartWrapper
      id={id}
      chartName='Energy sources'
      data={consumption!.map((c) => (c.percentage ? c.percentage : 0))}
      labels={consumption!.map((c) => (c.source ? c.source : ''))}
      type='bar'
    />
  );
};

export default EnergySourcesChart;
