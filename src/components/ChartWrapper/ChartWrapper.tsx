import { useSortable } from '@dnd-kit/sortable';
import { CSSProperties, Card, Text } from '@mantine/core';
import { CSS } from '@dnd-kit/utilities';
import Chart from 'react-apexcharts';

interface ChartWrapperProps {
  id: number;
  chartName: string;
  labels: string[];
  data: number[];
  type:
    | 'line'
    | 'area'
    | 'bar'
    | 'pie'
    | 'donut'
    | 'radialBar'
    | 'scatter'
    | 'bubble'
    | 'heatmap'
    | 'candlestick'
    | 'boxPlot'
    | 'radar'
    | 'polarArea'
    | 'rangeBar'
    | 'rangeArea'
    | 'treemap';
}

const ChartWrapper: React.FC<ChartWrapperProps> = ({ chartName, data, labels, type, id }) => {
  const options = {
    chart: { id: chartName },
    xaxis: { categories: [''] },
    labels: [''],
    colors: ['#04364A', '#176B87', '#64CCC5', '#91f2ec']
  };

  let series = [];

  const { isDragging, attributes, listeners, setNodeRef, transform, transition } = useSortable({
    id
  });

  const style: CSSProperties = {
    cursor: 'pointer',
    opacity: isDragging ? 0.5 : 1
  };

  if (transform && transition) {
    style.transform = CSS.Transform.toString(transform);
    style.transition = transition;
  }

  if (type === 'pie' || type === 'donut') {
    options.labels = labels;
    series = data;
  } else {
    options.xaxis.categories = labels;
    series = [
      {
        name: chartName,
        data
      }
    ];
  }

  return (
    <Card
      shadow='sm'
      padding='lg'
      radius='md'
      withBorder
      ref={setNodeRef}
      {...attributes}
      {...listeners}
      style={style}
    >
      <Text fw={500}>{chartName}</Text>
      <Chart options={options} series={series} type={type} />
    </Card>
  );
};

export default ChartWrapper;
