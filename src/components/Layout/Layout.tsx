import { AppShell, AppShellNavbar, rem } from '@mantine/core';
import Sidebar from '../Sidebar/Sidebar';
import { ReactNode } from 'react';

interface LayoutProps {
  children: ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  const session = sessionStorage.getItem('session');

  return (
    <AppShell navbar={{ width: rem(80), breakpoint: 'xs' }} padding='md'>
      {session ? (
        <AppShellNavbar>
          <Sidebar />
        </AppShellNavbar>
      ) : null}
      <AppShell.Main>{children}</AppShell.Main>
    </AppShell>
  );
};

export default Layout;
